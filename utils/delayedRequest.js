'use strict';

const rp = require('request-promise');
const sleep = require('system-sleep');
exports.request = (options, time) => {
  sleep(time);
  return rp(options);
};
